import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faPlusSquare, faTachometerAlt, faUserCircle, faQuestionCircle, faClone, faCodeBranch, faCheckSquare, faSearch, faHome, faRocket, faLockOpen } from '@fortawesome/free-solid-svg-icons'
import { faFileAlt, faCheckCircle } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faUserSecret, faPlusSquare, faTachometerAlt, faUserCircle, faQuestionCircle, faClone, faCodeBranch, faCheckSquare, faSearch, faHome, faFileAlt, faRocket, faLockOpen, faCheckCircle)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(BootstrapVue)

new Vue({
  el: '#app',
  render: h => h(App)
})
